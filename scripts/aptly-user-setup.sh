# create aptly user
set -ex

sudo groupadd -g 1111 aptly
sudo mkdir /srv/aptly
sudo useradd -u 1111 -g 1111 aptly -s /bin/bash -d /srv/aptly
sudo chown aptly:aptly -R /srv/aptly/
