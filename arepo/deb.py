from pathlib import Path
import re
import shutil
from subprocess import run, PIPE

from debian import deb822
from debian import changelog

from arepo.common import tempdir
from arepo import distro_info


def pbuilder_command(
    operation,
    distro=None,
    arch=None,
    basetgz=None,
    mirror=None,
    inputfiles=None,
    result_dir=None,
    opts=None,
    srcpkg=None,
    ):
    distro_name = distro_info.series2distro(distro)
    distro_cfg = distro_info.distro_config(
        distro_name=distro_name, distro_series=distro)
    if not mirror and 'mirror' in distro_cfg:
        mirror = distro_cfg['mirror']

    cmd = ['sudo', 'pbuilder', operation]
    if distro:
        cmd += ['--distribution', distro]
    if arch:
        cmd += ['--architecture', arch]
    if basetgz:
        cmd += ['--basetgz', str(basetgz)]
    if mirror:
        cmd += ['--mirror', mirror]
    if inputfiles:
        for inputfile in inputfiles:
            cmd += ['--inputfile', str(inputfile)]
    if result_dir:
        cmd += ['--buildresult', str(result_dir)]
    if opts:
        cmd += opts
    if srcpkg:
        cmd += [str(srcpkg)]
    return cmd


def copy_deb_srcpkg(srcpkg_path, out_path):
    changes_path = source_changes_from_srcpkg(srcpkg_path)
    if changes_path.exists():
        src_path = changes_path
    else:
        src_path = srcpkg_path
    o = run(['dcmd', src_path], stdout=PIPE, check=True)
    files = list(o.stdout.decode().splitlines(keepends=False))
    for fn in files:
        infile = Path(fn)
        outfile = out_path / infile.name
        print("copy: %s -> %s" % (infile, outfile))
        shutil.copy(infile, outfile)


def source_changes_from_srcpkg(srcpkg_path):
    changes_fn = re.sub(r'\.dsc$', '_source.changes', str(srcpkg_path))
    return Path(changes_fn)


def debian_archive_from_srcpkg(srcpkg_path):
    dsc = deb822.Dsc(srcpkg_path.open('rt'))
    for file in dsc['Files']:
        debian_archive_fn = file['name']
        if re.search(r'\.debian.tar.\w+$', debian_archive_fn):
            return srcpkg_path.parent / debian_archive_fn


def changelog_from_debian_archive(debian_archive_path, tmp_path=None):
    debian_archive_path = debian_archive_path.absolute()
    ch = None
    with tempdir('arepo-debian', tmp_path=tmp_path):
        cmd = ['tar', '-xf', debian_archive_path]
        run(cmd, check=True)
        f = open('debian/changelog', 'r')
        ch = changelog.Changelog(f, max_blocks=1)
    return ch


def distro_from_srcpkg(srcpkg_path, tmp_path=None):
    debian_archive_path = debian_archive_from_srcpkg(srcpkg_path)
    ch = changelog_from_debian_archive(debian_archive_path, tmp_path=tmp_path)
    return ch.distributions
