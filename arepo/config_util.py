from arepo import aaptly
from arepo.config import cfg
from arepo import distro_info
from arepo import ex


def get_repo_config(repo_id, check=True):
    repos = cfg('repos').to_dict()
    if repo_id not in repos:
        if check:
            raise ex.InvalidInput(msg="Config not found for repo: %s" % repo_id)
        return {}
    return parse_repo_config(repos[repo_id], repo_id)


def get_repo_configs(repo_list=None):
    result = {}
    repos = cfg('repos').to_dict()
    for repo_id, repo_cfg in repos.items():
        if repo_list and repo_id not in repo_list:
            continue

        result[repo_id] = parse_repo_config(repo_cfg, repo_id)

    return result


def parse_repo_config(repo_cfg, repo_id):
    rcfg = {}
    for key in ['name', 'main_pkg', 'build_repos']:
        if key in repo_cfg:
            rcfg[key] = repo_cfg[key]

    build_distros = []
    mirror_distros = []

    distros = {}
    mirrors = {}

    # parse distros
    if 'distros' in repo_cfg:
        build_distros = repo_cfg['distros']
    else:
        build_distros = cfg('defaults.distros').to_list()

    # parse archs
    for distro in build_distros:
        archs_cfg = {}
        if 'archs' in repo_cfg:
            archs_cfg = repo_cfg['archs']
        else:
            archs_cfg = cfg('defaults.archs', {})
        distros[distro] = {
            'archs': parse_distro_archs(archs_cfg, distro)
        }

    # parse mirrors
    if 'mirrors' in repo_cfg:
        mdistros = set()
        for mirror in repo_cfg['mirrors']:
            mcfg = get_mirror_config(mirror)
            mirrors[mirror] = mcfg
            mdistros.update(mcfg.get('distros', []))
        if mdistros:
            mirror_distros = distro_info.sort_series(mdistros)

    # all distros used in this repo
    all_distros = build_distros + mirror_distros
    all_distros = distro_info.sort_series(all_distros)

    # collect distro lists
    distro_lists = {
        'all': all_distros,
        'build': build_distros,
        'mirror': mirror_distros,
    }

    # consturct result config
    rcfg['distro_lists'] = distro_lists
    rcfg['distros'] = distros
    rcfg['mirrors'] = mirrors

    return rcfg


def parse_distro_archs(archs, distro):
    if distro in archs:
        # by distro series (bookworm, focal, ...)
        return [a for a in archs[distro]]

    distro_id = distro_info.series2distro(distro)
    if distro_id in archs:
        # by distro id (debian, ubuntu, ...)
        return [a for a in archs[distro_id]]

    if 'all' in archs:
        # by 'all' wildcard
        return [a for a in archs['all']]

    # no archs defined
    return []


def get_mirror_config(mirror, check=True):
    mirrors = cfg.get('mirrors')
    if mirror not in mirrors:
        if check:
            raise ex.InvalidInput(
                msg="Config not found for mirror: %s" % mirror)
        return {}
    return mirrors[mirror].to_dict()


def export_config():
    """
    export repos config in static stand-alone format

    good for integration in external apps
    """
    excfg = {}

    # repo configs (prased for export)
    exrepo_cfgs = export_repo_configs()
    excfg['repos'] = exrepo_cfgs

    # projects list (as is)
    projects = cfg('projects')
    if projects:
        projects = projects.to_dict()
        excfg['projects'] = projects

    # distros (generated for all exported distros)
    all_distros = set()
    for _, exrepo_cfg in exrepo_cfgs.items():
        dall = exrepo_cfg['distros'].keys()
        all_distros.update(dall)
    all_distros = distro_info.sort_series(all_distros)
    excfg['distros'] = export_distro_info(all_distros)

    # distros tree (generated for all exported distros)
    distros_tree = distro_info.distros_tree(all_distros)
    excfg['distros_tree'] = distros_tree

    return excfg


def export_repo_configs():
    exrepo_cfgs = {}
    repo_cfgs = get_repo_configs()
    for repo, repo_cfg in repo_cfgs.items():
        distros = {}
        for mdistro in reversed(repo_cfg['distro_lists']['all']):
            local_repo = aaptly.distro_repo(repo, mdistro)
            archs = aaptly.local_repo_archs(local_repo)
            distros[mdistro] = {
                'archs': archs,
            }

        exrepo_cfg = {
            'name': repo_cfg['name'],
            'distros': distros,
            'distros_list': repo_cfg['distro_lists']['all'],
            'main_pkg': repo_cfg['main_pkg'],
        }
        exrepo_cfgs[repo] = exrepo_cfg
    return exrepo_cfgs


def export_distro_info(distros):
    result = {}
    for distro in distros:
        distro_id, di = distro_info.series_info(distro)
        distro_name = distro_info.distro_id2name(distro_id)
        distro_cfg = {
            'id': distro_id,
            'name': distro_name,
            'version': di.version,
            'codename': di.codename,
            'series': di.series,
        }
        result[distro] = distro_cfg
    return result


def get_all_distro_repos():
    repo_cfgs = get_repo_configs()
    all_repos = []
    for repo, repo_cfg in repo_cfgs.items():
        distros = repo_cfg['distro_lists']['all']
        distro_repos = [aaptly.distro_repo(repo, d) for d in distros]
        all_repos += distro_repos
    return all_repos


def get_all_distro_mirrors():
    mirror_cfgs = cfg('mirrors').to_dict()
    all_mirrors = []
    for mirror, mirror_cfg in mirror_cfgs.items():
        distros = mirror_cfg.get('distros', [])
        distro_mirrors = [aaptly.distro_repo(mirror, d) for d in distros]
        all_mirrors += distro_mirrors
    return all_mirrors
