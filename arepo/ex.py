"""
arepo exceptions

when arepo CLI run results in exception being raised
its returncode is returned as process return code
"""


class ArepoException(Exception):
    msg_fmt = "An unknown error occurred"
    returncode = 1

    def __init__(self, msg=None, returncode=None, **kwargs):
        self.kwargs = kwargs
        if not msg:
            try:
                msg = self.msg_fmt.format(**kwargs)
            except Exception:
                # kwargs doesn't match those in message.
                # Returning this is still better than nothing.
                msg = self.msg_fmt
        if returncode is not None:
            self.returncode = returncode
        super().__init__(msg)


class QuietExit(ArepoException):
    msg_fmt = ""
    returncode = 0


class InvalidUsage(ArepoException):
    msg_fmt = "Invalid usage: {fail}"
    returncode = 10


class InvalidInput(ArepoException):
    msg_fmt = "Invalid input: {fail}"
    returncode = 11


class BuildImageNotFound(ArepoException):
    msg_fmt = ("Build image not found: {img}\n\n"
               "You can create it using:\n\n"
               "    arepo image create -d {distro} -a {arch}\n")
    returncode = 20


class MissingRequiredArgument(ArepoException):
    msg_fmt = "Missing required argument: {arg}"
    returncode = 30


class MissingRequiredArgumentConf(ArepoException):
    msg_fmt = ("Missing required argument: {arg}\n\n"
               "You can also use arepo config option: {conf}")
    returncode = 31


class MissingRequiredConfigOption(ArepoException):
    msg_fmt = "Missing required config option: {opt}"
    returncode = 34


class NoPackagesMatchingQuery(ArepoException):
    msg_fmt = "No packages matching query: {query}"
    returncode = 42


class UserAbort(ArepoException):
    msg_fmt = "Aborting per user request."
    returncode = 50


class UnexpectedCommandOutput(ArepoException):
    msg_fmt = "Unexpected command output: {out}"
    returncode = 64
