"""
arepo the repo management tool
"""
import os
import pkgutil
import sys
import subprocess
from logging import getLogger

import click

from arepo.__about__ import __version__
from arepo import commands
from arepo import ex
from arepo import log as _log


log = getLogger(__name__)


CLI_LOG_LEVELS = {
    'debug': _log.DEBUG,
    'verbose': _log.VERBOSE,
    'info': _log.INFO,
    'brief': _log.WARN,
    'quiet': _log.ERROR,
}


@click.group()
@click.option('-L', '--log-level',
              default='info', show_default=True,
              type=click.Choice(CLI_LOG_LEVELS.keys()),
              help="set log level")
@click.help_option('-h', '--help',
                   help="show this help message")
@click.version_option(__version__, message='%(version)s',
                      help="show arepo version")
def cli(log_level='info'):
    """
    arepo the packaging repo management tool
    """
    level = CLI_LOG_LEVELS[log_level]
    _log.set_log_level(level)
    log.verbose("arepo version: %s", __version__)
    log.verbose("log level: %s (%s)", log_level.upper(), _log.get_log_level())


def arepo(*args):
    """
    arepo shell interface
    """
    code = 0
    try:
        cli(args)
    except subprocess.CalledProcessError as e:
        print(e)
        code = e.returncode
    except ex.ArepoException as e:
        print(e)
        code = e.returncode

    return code


def main():
    """
    arepo console_scripts entry point
    """
    cargs = sys.argv[1:]
    sys.exit(arepo(*cargs))


def __load_commands():
    """
    load available arepo commands

    should only be called once on module load
    """
    pkgpath = os.path.dirname(commands.__file__)
    for _, modname, _ in pkgutil.iter_modules([pkgpath]):
        modpath = "arepo.commands.%s" % (modname)
        mod = __import__(modpath, fromlist=[''])
        cmds = getattr(mod, 'AREPO_CLI_COMMANDS', None)
        if not cmds:
            log.warning('command module with no CLI commands: %s', modpath)
            continue
        for cmd in cmds:
            cli.add_command(cmd)


__load_commands()


if __name__ == '__main__':
    main()
