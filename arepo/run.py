"""
run and log system commands

see primary run() function
"""
import subprocess
try:
    from shlex import join
except ImportError:
    from subprocess import list2cmdline as join

from arepo.log import getLogger


log = getLogger(__name__)


def run(cmd, *args, **kwargs):
    """
    subprocess run with command logging
    """
    cmd = list(map(str, cmd))
    log.command(join(cmd))
    return subprocess.run(cmd, *args, **kwargs)


def check_output(cmd):
    o = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if o.returncode != 0 or not o.stdout:
        return ''
    return o.stdout.decode()
