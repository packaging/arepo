import subprocess
import getpass

from arepo.config import cfg
from arepo.run import check_output


def command(args):
    cmd = ['aptly'] + list(args)
    user = cfg('aptly.user')
    if user:
        current_user = getpass.getuser()
        if current_user != user:
            # sudo to aptly user if set up
            cmd = ['sudo', '-H', '-u', user] + cmd
    return cmd


def distro_repo(repo, distro_series):
    return '%s_%s' % (repo, distro_series)


def distro_mirror(mirror, distro_series):
    return '%s_%s' % (mirror, distro_series)


def distro_prefix(repo, distro_series):
    # distro_name = distro_info.series2distro(distro_series)
    # return '%s/%s' % (repo, distro_name)
    return repo


def local_repos():
    """
    return a list of local aptly repos
    """
    cmd = command(['repo', 'list', '-raw'])
    return check_output(cmd).splitlines()


def local_mirrors():
    """
    return a list of local aptly mirrors
    """
    cmd = command(['mirror', 'list', '-raw'])
    return check_output(cmd).splitlines()


def local_repo_archs(local_repo):
    """
    query local aptly repo for present package architectures
    """
    archs = set({})

    cmd = command(['repo', 'search', local_repo])
    pkgs = check_output(cmd).splitlines()

    for pkg in pkgs:
        _, p, arch = pkg.rpartition('_')
        if p != '_':
            continue
        archs.add(arch)

    archs.discard('all')
    archs = sorted(archs)

    return archs


def search(local_repo, query='Name'):
    """
    search packages in local aptly repo
    """
    cmd = command(['repo', 'search', local_repo, query])
    return check_output(cmd).splitlines()
