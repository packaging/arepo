import csv
from collections import namedtuple

from arepo.config import cfg


DISTROS=['debian', 'ubuntu']

_distro_info = None


def info():
    global _distro_info
    if _distro_info is not None:
        return _distro_info

    _distro_info = {}
    for distro in DISTROS:
        _distro_info[distro] = get_distro_info_for_distro(distro)
    return _distro_info


def series_info(distro_series):
    i = info()
    for distro_name, sinfo in i.items():
        if distro_series in sinfo:
            return distro_name, sinfo[distro_series]
    return None, None


def series_full_name(distro_series):
    distro_name, si = series_info(distro_series)
    name = '%s %s %s' % (distro_name.capitalize(), si.version, si.codename)
    return name


def series_apkg_id(distro_series):
    """
    return distro id recognized by apkg
    """
    distro_name, si = series_info(distro_series)
    version, _, _ = si.version.partition(' ')
    return '%s-%s' % (distro_name, version)


def sort_series(series_list):

    def distro_key(distro):
        distro_prio = { 'debian': 5, 'ubuntu': 4 }
        dp = distro_prio.get(distro)
        if dp:
            return '~%s~%s' % (dp, distro)
        return distro

    def sort_key(series_info):
        dkey = distro_key(series_info[0])
        vkey = series_info[1].version
        return (dkey, vkey)

    sinfos = [series_info(s) for s in series_list]
    sinfos = sorted(sinfos, key=sort_key, reverse=True)
    series = [si[1].series for si in sinfos]
    return series


def distros_tree(series_list):
    tree = []
    series = []
    sinfos = [series_info(s) for s in sort_series(series_list)]
    last_distro = None

    for si in sinfos + [('_GUARD_', Dummy())]:
        distro = si[0]
        if distro != last_distro:
            if series:
                tree.append({
                    'id': last_distro,
                    'name': distro_id2name(last_distro),
                    'series': series,
                })
                series = []
            last_distro = distro
        series.append(si[1].series)

    return tree


def distro_config(distro_name=None, distro_series=None):
    dcfg = {}
    if distro_name:
        dcfg = cfg('distro_config.%s' % distro_name) or dcfg
    if distro_series:
        scfg = cfg('distro_config.%s' % distro_series)
        if scfg:
            dcfg.update(scfg)
    return dcfg


def series2distro(distro_series, distro_info=None):
    if not distro_info:
        distro_info = info()

    for key, val in distro_info.items():
        if distro_series in val:
            return key

    return None


def distro_id2name(distro_id):
    # works for Debian and Ubuntu
    # might need some special cases for i.e. linuxmint -> LinuxMint
    return distro_id.capitalize()


def get_distro_info_for_distro(distro='debian'):
    distro_info_file = '/usr/share/distro-info/%s.csv' % distro
    DistroInfoTuple = None
    info = {}
    with open(distro_info_file, 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        if not DistroInfoTuple:
            fields = list(map(lambda x: x.replace('-', '_'), fields))
            n_fields = len(fields)
            DistroInfoTuple = namedtuple('DistroInfoTuple', fields)

        for row in csvreader:
            while len(row) < n_fields:
                row.append(None)
            dit = DistroInfoTuple(*row)
            info[dit.series] = dit

    return info


class Dummy:
    def __getattr__(self, attr):
        return 'dummy'
