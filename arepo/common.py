from contextlib import contextmanager
import os
from pathlib import Path
import re
import shutil
import random

from arepo.config import cfg
from arepo import ex
from arepo.log import T


@contextmanager
def cd(newdir):
    """
    Temporarily change current directory.
    """
    olddir = os.getcwd()
    oldpwd = os.environ.get('PWD')
    newpath = os.path.abspath(os.path.expanduser(str(newdir)))
    os.chdir(newpath)
    os.environ['PWD'] = newpath
    try:
        yield
    finally:
        os.chdir(olddir)
        if oldpwd is not None:
            os.environ['PWD'] = oldpwd
        else:
            del os.environ['PWD']


@contextmanager
def tempdir(basename, tmp_path=None):
    """
    Temporarily change current directory into a temp dir to be removed.
    """
    if not tmp_path:
        tmp_path = Path.cwd()
    rnd = "%08x" % random.getrandbits(32)
    dir_name = "%s-%s" % (basename, rnd)
    target_path = tmp_path / dir_name
    target_path.mkdir(parents=True)
    with cd(target_path):
        yield
    shutil.rmtree(target_path)


def parse_list_arg(arg):
    result = []
    if not arg:
        return result
    for item in arg:
        if ' ' in item:
            for subitem in re.split('\s', item):
                result.append(subitem)
        else:
            result.append(item)
    return result


def parse_required_config_arg(name, val, conf=None, list_arg=False, path_arg=False):
    if val:
        if list_arg:
            val = parse_list_arg(val)
    else:
        val = cfg(name)
        if not val:
            raise ex.MissingRequiredArgumentConf(arg=name, conf=conf or name)

    if path_arg:
        val = Path(val).expanduser()

    return val


def get_basetgz_path(images_dir, distro, arch):
    basetgz_name = '%s-%s-base.tgz' % (distro, arch)
    return images_dir / basetgz_name


def color_mark(success):
    if success:
        return T.green("✓")
    else:
        return T.red("️✕")
