from dynaconf import Dynaconf
from pathlib import Path


SETTINGS_FILES = [
    Path('/etc/arepo.toml'),
    Path.home() / '.arepo.toml',
    Path('arepo.toml'),
]


cfg = Dynaconf(
    envvar_prefix="AREPO",
    settings_files=SETTINGS_FILES,
    core_loaders=['TOML'],
)
