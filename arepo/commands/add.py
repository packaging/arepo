from pathlib import Path

import click

from arepo import ex
from arepo import aaptly
from arepo import common
from arepo import config_util
from arepo.run import run


@click.command(name='add')
@click.argument('repo', type=str)
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--input-dir', '-I', type=Path,
              help="input directory with per-distro package dirs")
@click.help_option('-h', '--help', help='show this help')
def add(repo, distros=None, input_dir=None):
    """
    auto-add .deb packages to per-distro repos using aptly
    """
    if not input_dir:
        input_dir = Path()
    if distros:
        distros = common.parse_list_arg(distros)
    elif repo:
        repo_cfg = config_util.get_repo_config(repo)
        distros = repo_cfg.get('distros', {}).keys()
    else:
        distros = common.parse_required_config_arg(
            'defaults.distros', distros, list_arg=True)
    print("target distros: %s" % ' '.join(distros))

    found = False
    for distro in distros:
        drepo = aaptly.distro_repo(repo, distro)
        drepo_path = input_dir / distro
        pkgs = drepo_path.glob('*.deb')
        pkgs = list(map(lambda x: x.absolute(), pkgs))
        if not pkgs:
            continue
        args = ['repo', 'add', drepo]
        args += list(map(str, pkgs))
        cmd = aaptly.command(args)
        run(cmd)
        found = True

    if not found:
        msg = "No packages found in distro dirs: %s" % ', '.join(distros)
        raise ex.InvalidUsage(msg=msg)


AREPO_CLI_COMMANDS = [add]
