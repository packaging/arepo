import click
from collections import defaultdict
import re

from arepo import aaptly
from arepo import common
from arepo import config_util
from arepo import ex
from arepo.log import getLogger, T


log = getLogger(__name__)


OUTPUT_FORMATS = ['full', 'short', 'version', 'arch', 'count']


@click.command()
@click.argument('package-query', required=False)
@click.option('--repos', '-r', multiple=True,
              help='only list selected repos')
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--format', '-f',
              type=click.Choice(OUTPUT_FORMATS),
              default='short', show_default=True,
              help="select output format")
@click.help_option('-h', '--help', help='show this help')
def search(package_query=None, repos=None, distros=None, format=''):
    """
    search packages in local repos matching aptly query

    Use --format to select from various output formats.
    """
    if not package_query:
        package_query = 'Name'
    repos = common.parse_list_arg(repos)
    distros = common.parse_list_arg(distros)

    repo_pkgs = search_repos(package_query, repos=repos, distros=distros)
    for local_repo, pkgs in repo_pkgs.items():
        print_found_pkgs(local_repo, pkgs, format)


def search_repos(package_query, repos=None, distros=None, check=True,
                 key_format='local_repo'):
    repo_cfgs = config_util.get_repo_configs()
    arepos = aaptly.local_repos()
    results = {}
    for repo, rcfg in repo_cfgs.items():
        if repos and repo not in repos:
            continue
        for distro in rcfg['distro_lists']['all']:
            if distros and distro not in distros:
                continue
            local_repo = aaptly.distro_repo(repo, distro)
            if local_repo not in arepos:
                continue
            pkgs = aaptly.search(local_repo, query=package_query)
            if not pkgs:
                continue
            if key_format == 'distro':
                key = distro
            else:
                key = local_repo
            results[key] = pkgs
    if check and not results:
        raise ex.NoPackagesMatchingQuery(query=package_query)
    return results


def print_found_pkgs(local_repo, pkgs, format):
    if format == 'count':
        print("%s: %s" % (T.bold(local_repo), len(pkgs)))
    elif format == 'short':
        pkg_archs, all_archs = group_pkgs_archs(pkgs)
        print("# %s (%s packages)" % (T.bold(local_repo), len(pkgs)))
        for pkg, archs in pkg_archs.items():
            print("%s [%s]" % (pkg, " ".join(archs)))
    elif format == 'version':
        print("# %s" % T.bold(local_repo))
        versions = set([package_version(pkg) for pkg in pkgs])
        versions = sorted(versions, reverse=True)
        for ver in versions:
            print(ver)
    elif format == 'arch':
        print("# %s" % T.bold(local_repo))
        archs = count_pkgs_per_arch(pkgs)
        for arch, n in archs.items():
            print("%s: %s" % (arch, n))
    elif format == 'full':
        print("# %s (%s packages)" % (T.bold(local_repo), len(pkgs)))
        for pkg in pkgs:
            print(pkg)
    else:
        raise ex.InvalidInput(
            msg="Invalid search output format: %s" % format)


def package_version(pkg):
    m = re.match(r'^[^_]+_(\d[^~_]*)', pkg)
    if not m:
        return None
    return m.group(1)


def package_arch(pkg):
    p, _, arch = pkg.rpartition('_')
    return p, arch


def group_pkgs_archs(pkgs):
    pkg_archs = defaultdict(set)
    all_archs = set()
    for pkg in sorted(pkgs, reverse=True):
        pkg_base, arch = package_arch(pkg)
        pkg_archs[pkg_base].add(arch)
        all_archs.add(arch)
    for pkg in pkg_archs:
        pkg_archs[pkg] = sorted(pkg_archs[pkg])
    return pkg_archs, sorted(all_archs)


def count_pkgs_per_arch(pkgs):
    archs = defaultdict(int)
    for pkg in pkgs:
        _, arch = package_arch(pkg)
        archs[arch] += 1
    return dict(sorted(archs.items()))


AREPO_CLI_COMMANDS = [search]
