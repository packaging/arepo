import click

from arepo import aaptly
from arepo.config import cfg
from arepo import config_util
from arepo import common
from arepo.log import getLogger, T
from arepo.run import run


log = getLogger(__name__)


@click.group(name='mirror')
@click.help_option('-h', '--help', help='show command help')
def cli_mirror():
    """
    manage per-distro mirrors using aptly
    """


@cli_mirror.command()
@click.argument('mirror', type=str)
@click.help_option('-h', '--help', help='show command help')
def create(mirror):
    """
    create per-distro mirrors
    """
    mcfg = config_util.get_mirror_config(mirror)

    for distro in mcfg['distros']:
        dmirror = aaptly.distro_mirror(mirror, distro)
        args = ['mirror', 'create']
        components = mcfg.get('components')
        if components:
            # main/debug component isn't in LaunchPad Release file
            args.append('-force-components')
        args += [dmirror, mcfg['url'], distro]
        if components:
            args += components
        cmd = aaptly.command(args)
        run(cmd, check=False)


@cli_mirror.command()
@click.argument('mirror', type=str)
@click.help_option('-h', '--help', help='show command help')
def update(mirror):
    """
    update per-distro mirrors
    """
    mcfg = config_util.get_mirror_config(mirror)

    for distro in mcfg['distros']:
        dmirror = aaptly.distro_mirror(mirror, distro)
        args = ['mirror', 'update', dmirror]
        cmd = aaptly.command(args)
        run(cmd, check=False)


@cli_mirror.command()
@click.argument('mirror', type=str)
@click.help_option('-h', '--help', help='show command help')
def drop(mirror):
    """
    [!] delete per-distro mirrors
    """
    mcfg = config_util.get_mirror_config(mirror)

    for distro in mcfg['distros']:
        dmirror = aaptly.distro_mirror(mirror, distro)
        args = ['mirror', 'drop', dmirror]
        cmd = aaptly.command(args)
        run(cmd, check=False)


@cli_mirror.command()
@click.option('-a', '--aptly', is_flag=True,
              help='list aptly mirrors')
@click.option('--alien', is_flag=True,
              help='list aptly mirrors not used by arepo')
@click.help_option('-h', '--help', help='show this help')
def list(aptly=False, alien=False):
    """
    list mirrors
    """
    if aptly:
        cmd = aaptly.command(['mirror', 'list'])
        run(cmd, check=True)
    elif alien:
        all_mirrors = config_util.get_all_distro_mirrors()
        amirrors = aaptly.local_mirrors()
        log.info("listing aptly mirrors not used by arepo")
        aliens = [m for m in amirrors if m not in all_mirrors]
        print("\n".join(aliens))
    else:
        mirrors = cfg.get('mirrors', {})
        amirrors = aaptly.local_mirrors()
        for mid, mcfg in mirrors.items():
            print("- %s: %s" % (T.bold(mid), mcfg.get('url')))
            for distro in mcfg.get('distros', []):
                dmirror = aaptly.distro_mirror(mid, distro)
                mark = common.color_mark(dmirror in amirrors)
                print("   %s %s" % (mark, dmirror))


AREPO_CLI_COMMANDS = [cli_mirror]
