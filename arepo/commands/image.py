import click

from pathlib import Path

from arepo import ex
from arepo import common
from arepo import config_util
from arepo import deb
from arepo.log import getLogger
from arepo.run import run


log = getLogger(__name__)


@click.command(name='image')
@click.argument('operation', nargs=1,
                type=click.Choice(['create', 'update', 'list']))
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--archs', '-a', multiple=True,
              help="target specified architectures")
@click.option('--repo', '-r', type=str,
              help="get archs and distros from repo config")
@click.option('--images-dir', '-i', type=Path,
              help="base dir for build images")
@click.help_option('-h', '--help', help='show this help')
def image(operation, archs=None, distros=None, repo=None, images_dir=None):
    """
    create/update build images (base.tgz) using pbuilder
    """
    log.bold("%s build images", operation)
    archs = common.parse_list_arg(archs)
    distros = common.parse_list_arg(distros)
    images_dir = common.parse_required_config_arg(
        'arepo.images_dir', images_dir, path_arg=True)

    if repo:
        repo_cfg = config_util.get_repo_config(repo)
        if not distros:
            # distros from repo config
            distros = repo_cfg.get('distros', {}).keys()

    if not distros:
        raise ex.MissingRequiredArgument(arg='--distros or --repo')
    if not (archs or repo):
        raise ex.MissingRequiredArgument(arg='--archs or --repo')

    if operation == 'create':
        images_dir.mkdir(parents=True, exist_ok=True)

    for distro in distros:
        if repo:
            # archs from repo
            archs = repo_cfg.get('distros', {}).get(distro, {}).get('archs', [])
            if not archs:
                msg = "Can't find archs in %s repo config for %s" % (repo, distro)
                raise ex.MissingRequiredConfigOption(msg=msg)

        for arch in archs:
            basetgz_path = common.get_basetgz_path(images_dir, distro, arch)

            if operation == 'list':
                print(basetgz_path)
                continue

            log.info("%s %s %s image: %s", operation, distro, arch, basetgz_path)

            cmd = deb.pbuilder_command(
                operation, distro=distro, arch=arch, basetgz=basetgz_path)

            o = run(cmd)
            if o.returncode == 0:
                log.success("%sd image: %s", operation, basetgz_path)
            else:
                log.error("failed to %s image: %s", operation, basetgz_path)
                o.check_returncode()


AREPO_CLI_COMMANDS = [image]
