import click

from arepo import aaptly
from arepo import common
from arepo import config_util
from arepo.run import run
from arepo.log import getLogger, T


log = getLogger(__name__)


@click.command(name='list')
@click.option('-a', '--aptly', is_flag=True,
              help='list local aptly repos')
@click.option('--alien', is_flag=True,
              help='list local aptly repos not used by arepo')
@click.option('-r', '--repos', type=str, multiple=True,
              help='only list selected repos')
@click.help_option('-h', '--help', help='show this help')
def list(aptly=False, alien=False, repos=None):
    """
    list repos
    """
    repos = common.parse_list_arg(repos)
    if aptly:
        cmd = aaptly.command(['repo', 'list'])
        run(cmd, check=True)
    elif alien:
        all_repos = config_util.get_all_distro_repos()
        arepos = aaptly.local_repos()
        log.info("listing aptly repos not used by arepo")
        aliens = [r for r in arepos if r not in all_repos]
        print("\n".join(aliens))
    else:
        repo_cfgs = config_util.get_repo_configs()
        arepos = aaptly.local_repos()
        for repo, rcfg in repo_cfgs.items():
            if repos and repo not in repos:
                continue
            print("- %s: %s" % (T.bold(repo), rcfg.get('name')))
            for distro in rcfg['distro_lists']['all']:
                local_repo = aaptly.distro_repo(repo, distro)
                prefix = aaptly.distro_prefix(repo, distro)
                mark = common.color_mark(local_repo in arepos)
                print("  %s %s -> %s" % (mark, local_repo, prefix))


AREPO_CLI_COMMANDS = [list]
