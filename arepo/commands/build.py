import multiprocessing as mp
from pathlib import Path
import subprocess
import queue

import click

from arepo import ex
from arepo import config_util
from arepo import common
from arepo import deb
from arepo.log import getLogger, T
from arepo.run import join


log = getLogger(__name__)


@click.command(name='build')
@click.argument('srcpkgs', type=Path, nargs=-1)
@click.option('--archs', '-a', multiple=True,
              help="build for specified architectures")
@click.option('--repo', '-r', type=str,
              help="get archs from selected repo config")
@click.option('--images-dir', '-i', type=Path,
              help="base dir for build images")
@click.option('--result-dir', '-O', type=Path,
              help="output results into specified dir")
@click.option('--max-jobs', '-j', type=int,
              help="maximum number of parallel build jobs")
@click.help_option('-h', '--help', help='show this help')
def build(srcpkgs, archs=None, repo=False, images_dir=None,
          result_dir=None, max_jobs=None):
    """
    build packages from source packages using pbuilder
    """
    log.bold("build packages")
    images_dir = common.parse_required_config_arg(
        'arepo.images_dir', images_dir, path_arg=True)
    archs = common.parse_list_arg(archs)
    if repo:
        archs_str = 'from %s repo config' % repo
        repo_cfg = config_util.get_repo_config(repo)
    else:
        archs_str = ' '.join(archs)
        if not archs:
            raise ex.MissingRequiredArgument(arg='--archs or --repo')

    log.info("target archs: %s" % archs_str)

    if not srcpkgs:
        cwd = Path.cwd()
        srcpkgs = list(cwd.glob('*.dsc'))
        if srcpkgs:
            log.info("auto-selecting %s source packages"
                     " in cwd: %s/*.dsc" % (len(srcpkgs), cwd))
        else:
            raise ex.InvalidUsage(
                msg=("no input source packages specified"
                     " and none found in cwd: %s/*.dsc" % cwd))

    log.info("building %s source packages:" % len(srcpkgs))
    for srcpkg in srcpkgs:
        log.info("  %s" % srcpkg)

    if not result_dir:
        result_dir = Path()

    jobs = []
    jid_last = 0
    for srcpkg in srcpkgs:
        log.verbose("building source package: %s" % srcpkg)

        distro = deb.distro_from_srcpkg(srcpkg, tmp_path=result_dir)

        out_path = result_dir / distro
        out_path.mkdir(parents=True, exist_ok=True)
        inputfiles = []

        if repo:
            # per-distro archs from repo config
            if not archs:
                distros_cfg = repo_cfg.get('distros', {})
                archs = distros_cfg.get(distro, {}).get('archs', [])
                if not archs:
                    msg = "Can't find archs in %s repo config for %s" % (repo, distro)
                    raise ex.MissingRequiredConfigOption(msg=msg)
                log.verbose("archs from %s repo config for %s: %s"
                            % (repo, distro, ' '.join(archs)))

            # build_repos support
            build_repos_sources = get_build_repos_sources(repo_cfg.get('build_repos'), distro)
            if build_repos_sources:
                build_repos_inputfile = out_path / 'inputfiles' / 'build_repos.list'
                log.info("build repos sources file: %s:\n%s", build_repos_inputfile,
                        build_repos_sources)
                build_repos_inputfile.parent.mkdir(parents=True, exist_ok=True)
                with build_repos_inputfile.open('wt') as f:
                    f.write(build_repos_sources)
                inputfiles.append(build_repos_inputfile)

        for arch in archs:
            basetgz_path = common.get_basetgz_path(images_dir, distro, arch)
            if not basetgz_path.exists():
                raise ex.BuildImageNotFound(
                    img=basetgz_path, distro=distro, arch=arch)

            log_path = out_path / ('%s_%s.buildlog' % (srcpkg.stem, arch))

            cmd = deb.pbuilder_command(
                'build',
                distro=distro,
                arch=arch,
                basetgz=basetgz_path,
                inputfiles=inputfiles,
                result_dir=out_path,
                srcpkg=srcpkg)

            jid_last += 1
            job_name = "%s @ %s-%s" % (srcpkg.name, distro, arch)
            bj = BuildJob(jid_last, job_name, cmd, log_path=log_path)
            jobs.append(bj)

    r = run_build_jobs(jobs, max_jobs=max_jobs)
    if not r:
        # return non-zero when a job failed
        raise ex.QuietExit(returncode=1)


def get_build_repos_sources(build_repos, distro):
    if not build_repos:
        return None

    sources = ''
    for br in build_repos:
        sources += '%s\n' % get_build_repo_source(br, distro)

    return sources


def get_build_repo_source(build_repo, distro):
    url = build_repo['url']
    components = build_repo.get('components', ['main'])
    options = build_repo.get('options', {})

    source = 'deb '
    if options:
        opts = []
        for opt, val in options.items():
            opts.append('%s=%s' % (opt, val))
        options_str = '[%s] ' % " ".join(opts)
        source += options_str

    source += '%s %s %s' % (url, distro, " ".join(components))
    return source


def run_build_jobs(jobs, max_jobs=None):
    if not max_jobs:
        max_jobs = 4

    q = mp.Queue()
    jobs_queue = list(jobs)
    jobs_active = []
    jobs_done = []

    job_check_period = 2.0

    updated = False

    while True:

        while jobs_queue and len(jobs_active) < max_jobs:
            job = jobs_queue.pop(0)
            job.prc = mp.Process(target=build_job, args=(job, q))
            job.prc.start()
            job.active = True
            jobs_active.append(job)
            log.bold("STARTED %s" % job)
            log.command(job.cmd_str())
            updated = True

        if updated:
            print_jobs_status(jobs)
            updated = False

        if not jobs_active:
            log.bold("all build jobs finished!")
            break

        try:
            msg = q.get(True, job_check_period)
            # job sent a FINISHED message, processed below
            _, jid, returncode = msg
            job = get_job_by_jid(jobs, jid)
            job_finished(job, returncode)
            jobs_active.remove(job)
            jobs_done.append(job)
            updated = True
        except queue.Empty:
            pass

        jobs_active, jobs_finished = check_active_jobs(jobs_active)
        if jobs_finished:
            for job in jobs_finished:
                job_finished(job)
                jobs_done.append(job)
            updated = True

    for job in jobs:
        if not job.is_ok():
            return False

    return True


def check_active_jobs(jobs):
    jobs_active = []
    jobs_finished = []
    for job in jobs:
        if job.active and job.prc.is_alive():
            jobs_active.append(job)
        else:
            jobs_finished.append(job)
    return jobs_active, jobs_finished


def split_jobs(jobs):
    jobs_queue = []
    jobs_active = []
    jobs_ok = []
    jobs_fail = []
    for job in jobs:
        if job.active:
            jobs_active.append(job)
        else:
            if job.is_queued():
                jobs_queue.append(job)
            elif job.is_ok():
                jobs_ok.append(job)
            else:
                jobs_fail.append(job)
    return jobs_active, jobs_queue, jobs_ok, jobs_fail


def print_jobs_status(jobs):
    jobs_active, jobs_q, jobs_ok, jobs_fail = split_jobs(jobs)
    print()
    if jobs_active:
        print("%s running jobs:" % len(jobs_active))
        for job in jobs_active:
            print(T.bold("  %s" % job))
    if jobs_q:
        print("%s queued jobs:" % len(jobs_q))
        for job in jobs_q:
            print("  %s" % job)
    if jobs_ok:
        print("%s successful jobs:" % len(jobs_ok))
        for job in jobs_ok:
            print(T.success("  %s" % job))
    if jobs_fail:
        print("%s failed jobs:" % len(jobs_fail))
        for job in jobs_fail:
            print(T.error("  %s" % job))
            print("    log: %s" % job.log_path)
    print()


def job_finished(job, returncode=None):
    job.finish(returncode)

    if job.returncode == 0:
        log.success("SUCCESSFUL %s", job)
    else:
        if returncode is None:
            log.error("DEAD %s  (return code: %s)", job, job.returncode)
        else:
            log.error("FAILED %s  (return code: %s)", job, job.returncode)


def get_job_by_jid(jobs, jid):
    for j in jobs:
        if j.jid == jid:
            return j
    return None


def build_job(job, q):
    if job.log_path:
        logf = job.log_path.open('w')
        o = subprocess.run(job.cmd, stdout=logf, stderr=logf)
    else:
        o = subprocess.run(job.cmd)
    q.put(["FINISH", job.jid, o.returncode])


class BuildJob:
    def __init__(self, jid, name, cmd, log_path=None):
        self.jid = jid
        self.name = name
        self.cmd = cmd
        self.log_path = log_path
        self.prc = None
        self.active = False
        self.died = False
        self.returncode = None

    def finish(self, returncode=None):
        self.active = False
        if returncode is None:
            self.died = True
            self.returncode = self.prc.exitcode
        else:
            self.returncode = returncode

    def is_queued(self):
        return not self.active and self.returncode is None

    def is_ok(self):
        return self.returncode == 0

    def cmd_str(self):
        return join(self.cmd)

    def __str__(self):
        return 'BUILD JOB %s: %s' % (self.jid, self.name)


AREPO_CLI_COMMANDS = [build]
