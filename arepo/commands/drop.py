import click

from arepo import aaptly
from arepo.config_util import get_repo_config
from arepo.run import run


@click.command(name='drop')
@click.argument('repo', type=str)
@click.help_option('-h', '--help', help='show this help')
def drop(repo):
    """
    [!] delete per-distro repos using aptly
    """
    repo_cfg = get_repo_config(repo)

    for distro in repo_cfg['distro_lists']['build']:
        local_repo = aaptly.distro_repo(repo, distro)
        args = ['repo', 'drop', local_repo]
        cmd = aaptly.command(args)
        run(cmd, check=False)


AREPO_CLI_COMMANDS = [drop]
