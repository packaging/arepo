import json
import toml
import yaml

import click

from arepo.config import cfg, SETTINGS_FILES
from arepo import common
from arepo import config_util
from arepo.log import getLogger


log = getLogger(__name__)


@click.command(name='config')
@click.option('-f', '--format',
              type=click.Choice(['yaml', 'json', 'toml']),
              default='yaml', show_default=True,
              help="select output format")
@click.option('-e', '--export', is_flag=True,
              help="export parsed config for easy external use")
@click.option('-r', '--repo', multiple=True,
              help="show parsed repo config for REPO (or 'all')")
@click.help_option('-h', '--help', help='show this help')
def config(format, export, repo):
    """
    print/export arepo configuration as YAML/JSON/TOML
    """
    repos = common.parse_list_arg(repo)

    log_settings_files()

    cfg_data = {}
    if export:
        # show exported static config for easy external use
        cfg_data = config_util.export_config()
    elif repos:
        if 'all' in repos:
            repos = None
        # show repo config
        cfg_data = config_util.get_repo_configs(repos)
    else:
        # show full config
        cfg_data = cfg.as_dict()

    if format == 'toml':
        print(toml.dumps(cfg_data))
    elif format == 'json':
        print(json.dumps(cfg_data, indent=2))
    else:
        print(yaml.dump(cfg_data))


def log_settings_files():
    log.info("arepo settings files:")
    for sf in SETTINGS_FILES:
        exists_str = "exists" if sf.exists() else "doesn't exist"
        log.info("  %s  (%s)" % (sf, exists_str))


AREPO_CLI_COMMANDS = [config]
