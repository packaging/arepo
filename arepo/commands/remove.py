import click
from collections import defaultdict
import re

from arepo import aaptly
import arepo.commands.search as cmd_search
from arepo import common
from arepo import ex
from arepo.run import run
from arepo.log import getLogger, T


log = getLogger(__name__)


@click.command()
@click.argument('package-query', required=True)
@click.option('--repos', '-r', multiple=True,
              help='only remove from selected repos')
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--format', '-f',
              type=click.Choice(cmd_search.OUTPUT_FORMATS),
              default='count', show_default=True,
              help="select output format")
@click.option('--ask/--no-ask', default=True,
              help='ask for confirmation before removal')
@click.help_option('-h', '--help', help='show this help')
def remove(package_query=None, repos=None, distros=None, format=None, ask=True):
    """
    [!] remove packages in local repos matching aptly query

    Use `arepo search` to fine-tune your package query.
    """
    repos = common.parse_list_arg(repos)
    distros = common.parse_list_arg(distros)

    repo_pkgs = cmd_search.search_repos(
        package_query=package_query,
        repos=repos,
        distros=distros)

    if ask:
        n_pkgs = sum(len(p) for p in repo_pkgs.values())
        n_repos = len(repo_pkgs)
        print("%s packages to be REMOVED from %s local repos:\n" % (
            n_pkgs, n_repos))

        for local_repo, pkgs in repo_pkgs.items():
            cmd_search.print_found_pkgs(local_repo, pkgs, format=format)

        answer = input("\nDo you want to REMOVE these packages? [Y/n] ")
        if answer.lower() not in ['', 'y', 'yes']:
            raise ex.UserAbort

    for local_repo, pkgs in repo_pkgs.items():
        cmd = aaptly.command(['repo', 'remove', local_repo, package_query])
        run(cmd)


AREPO_CLI_COMMANDS = [remove]
