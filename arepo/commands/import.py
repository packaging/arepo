import click

from arepo import aaptly
from arepo.commands.mirror import update as mirror_update
from arepo import common
from arepo import config_util
from arepo.log import getLogger
from arepo.run import run


log = getLogger(__name__)


@click.command(name='import')
@click.argument('repo', type=str)
@click.option('--mirrors', '-m', type=str, multiple=True,
              help="select mirrors to sync  [default: repo config]")
@click.option('--package-query', '-q', type=str,
              default='Name', show_default=True,
              help="aptly package query for import")
@click.option('--update/--no-update', default=True,
              help="update aptly mirrors before importing")
@click.help_option('-h', '--help', help='show this help')
def cli_import(repo, mirrors=None, package_query='Name', update=True):
    """
    import packages from mirrors into repos
    """
    log.bold("import packages from mirrors")
    repo_cfg = config_util.get_repo_config(repo)
    mirrors_cfg = repo_cfg['mirrors']
    mirrors = common.parse_list_arg(mirrors)
    if not mirrors:
        mirrors = repo_cfg['mirrors'].keys()

    for mirror in mirrors:
        mcfg = mirrors_cfg.get(mirror)
        if not mcfg:
            log.warn("importing from %s mirror not in %s repo config", mirror, repo)
            mcfg = config_util.get_mirror_config(mirror)

        if update:
            log.info("updating %s mirror before import...", mirror)
            mirror_update.callback(mirror)

        log.info("importing from %s mirror into %s repo...", mirror, repo)
        for distro in mcfg['distros']:
            drepo = aaptly.distro_repo(repo, distro)
            dmirror = aaptly.distro_mirror(mirror, distro)

            log.info("  import: %s  ->  %s", dmirror, drepo)
            args = ['repo', 'import', dmirror, drepo, package_query]
            cmd = aaptly.command(args)
            run(cmd, check=False)


AREPO_CLI_COMMANDS = [cli_import]
