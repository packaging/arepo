import click

from arepo import aaptly
import arepo.commands.search as cmd_search
from arepo import common
from arepo import config_util
from arepo import ex
from arepo.run import run
from arepo.log import getLogger, T


log = getLogger(__name__)


OUTPUT_FORMATS = ['full', 'short', 'version', 'arch', 'count']


@click.command()
@click.argument('src-repo')
@click.argument('dst-repo')
@click.argument('package-query', required=False)
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--format', '-f',
              type=click.Choice(OUTPUT_FORMATS),
              default='short', show_default=True,
              help="select output format")
@click.option('--ask/--no-ask', default=True,
              help='ask for confirmation before removal')
@click.help_option('-h', '--help', help='show this help')
def copy(src_repo, dst_repo, package_query=None, distros=None, format='', ask=False):
    """
    copy packages between local repos matching aptly query

    Use `arepo search` to fine-tune your package query.
    """
    if not package_query:
        package_query = 'Name'
    distros = common.parse_list_arg(distros)

    distros = common_repo_distros(src_repo, dst_repo, distros=distros)

    src_pkgs = cmd_search.search_repos(
        package_query=package_query,
        repos=[src_repo],
        distros=distros,
        key_format='distro')
    distros = list(src_pkgs.keys())

    if ask:
        n_pkgs = sum(len(p) for p in src_pkgs.values())
        print(f"{T.bold}{n_pkgs}{T.normal} packages "
              f"to be {T.bold}COPIED{T.normal} from "
              f"{T.bold}{src_repo}{T.normal} "
              f"to {T.bold}{dst_repo}{T.normal}:\n")

        for local_repo, pkgs in src_pkgs.items():
            cmd_search.print_found_pkgs(local_repo, pkgs, format=format)

        answer = input("\nDo you want to COPY these packages? [Y/n] ")
        if answer.lower() not in ['', 'y', 'yes']:
            raise ex.UserAbort

    for distro in distros:
        src_arepo = aaptly.distro_repo(src_repo, distro)
        dst_arepo = aaptly.distro_repo(dst_repo, distro)
        cmd = aaptly.command(['repo', 'copy', src_arepo, dst_arepo, package_query])
        run(cmd)


def common_repo_distros(*repos, distros=None):
    all_repo_cfgs = config_util.get_repo_configs()
    arepos = aaptly.local_repos()

    repo_cfgs = {}
    for repo in repos:
        repo_cfg = all_repo_cfgs.get(repo)
        if not repo_cfg:
            raise ex.InvalidInput(fail=f"Config not found for repo: {repo}")
        repo_cfgs[repo] = repo_cfg

    if not distros:
        first_repo_cfg = repo_cfgs[repos[0]]
        distros = first_repo_cfg['distro_lists']['all']

    for repo, rcfg in repo_cfgs.items():
        common_distros = []
        for distro in rcfg['distro_lists']['all']:
            if distros and distro not in distros:
                continue
            local_repo = aaptly.distro_repo(repo, distro)
            if local_repo not in arepos:
                continue
            common_distros.append(distro)
        distros = common_distros
    return distros


AREPO_CLI_COMMANDS = [copy]
