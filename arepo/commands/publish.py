import click

from arepo import aaptly
from arepo.config_util import get_repo_config
from arepo.run import run


@click.command(name='publish')
@click.argument('repo', type=str)
@click.option('--init', is_flag=True,
              help="initial repos publish (only first time)")
@click.option('--skip-signing', is_flag=True,
              help="skip GPG signing of the repo (not recommended)")
@click.help_option('-h', '--help', help='show this help')
def publish(repo, init=False, skip_signing=False):
    """
    publish per-distro repos using aptly

    Use --init argument when publishing for first time.
    """
    repo_cfg = get_repo_config(repo)

    for distro in repo_cfg['distro_lists']['all']:
        local_repo = aaptly.distro_repo(repo, distro)
        prefix = aaptly.distro_prefix(repo, distro)
        if init:
            args = ['publish', 'repo']
            if skip_signing:
                args += ['-skip-signing']
            args += [local_repo, prefix]
        else:
            args = ['publish', 'update']
            if skip_signing:
                args += ['-skip-signing']
            args += [distro, prefix]
        cmd = aaptly.command(args)
        run(cmd, check=False)


AREPO_CLI_COMMANDS = [publish]
