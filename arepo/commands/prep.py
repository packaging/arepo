import click
from pathlib import Path
import shutil
import subprocess

from debian import deb822
from debian import changelog

from arepo import ex
from arepo import deb
from arepo import config_util
from arepo import common
from arepo import distro_info
from arepo.run import run


DEFAULT_BUILD_PATH=Path('build')
DEFAULT_MIRRORS_PATH=Path('.')


@click.group(name='prep')
@click.help_option('-h', '--help', help='show command help')
def cli_prep():
    """
    prepare per-distro source packages for build and/or mirrors
    """


@cli_prep.command()
@click.option('--srcpkg', '-s', type=Path,
              help="use this source package as input")
@click.option('--apkg', '-a', type=Path,
              help="use this apkg-managed project as input")
@click.option('--repo', '-r', type=str, required=True,
              help="get distros from selected repo config")
@click.option('--build-dir', '-B', type=Path,
              default=DEFAULT_BUILD_PATH, show_default=True,
              help="output source packages for build into this dir")
@click.option('--mirrors-dir', '-M', type=Path,
              default=DEFAULT_MIRRORS_PATH, show_default=True,
              help="output source packages for mirrors into this dir")
@click.help_option('-h', '--help', help='show this help')
def all(srcpkg=None, apkg=None, repo=None, build_dir=None, mirrors_dir=None):
    """
    prepare all per-distro source packages for build and mirrors
    """
    if not (srcpkg or apkg):
        raise ex.InvalidInput(msg="Please use '--srcpkg' or '--apkg' option to select input.")

    build.callback(srcpkg=srcpkg, apkg=apkg, repo=repo, build_dir=build_dir)
    mirrors.callback(srcpkg=srcpkg, apkg=apkg, repo=repo, mirrors_dir=mirrors_dir)


@cli_prep.command()
@click.option('--srcpkg', '-s', type=Path,
              help="use this source package as input")
@click.option('--apkg', '-a', type=Path,
              help="use this apkg-managed project as input")
@click.option('--distros', '-d', multiple=True,
              help="target specified distros")
@click.option('--repo', '-r', type=str,
              help="get distros from selected repo config")
@click.option('--build-dir', '-B', type=Path,
              default=DEFAULT_BUILD_PATH, show_default=True,
              help="output source packages for build into this dir")
@click.help_option('-h', '--help', help='show this help')
def build(srcpkg=None, apkg=None, distros=None, repo=None, build_dir=None):
    """
    prepare per-distro source packages for build
    """
    if not (srcpkg or apkg):
        raise ex.InvalidInput(msg="Please use '--srcpkg' or '--apkg' option to select input.")

    if distros:
        distros = common.parse_list_arg(distros)
    elif repo:
        repo_cfg = config_util.get_repo_config(repo)
        distros = repo_cfg.get('distros', {}).keys()
    else:
        distros = common.parse_required_config_arg(
            'defaults.distros', distros, list_arg=True)
    print("target distros: %s" % ' '.join(distros))

    if build_dir:
        build_dir.mkdir(parents=True, exist_ok=True)
    else:
        build_dir = DEFAULT_BUILD_PATH

    for distro in distros:
        if apkg:
            srcpkg = get_apkg_srcpkg(distro, apkg)
        prep_srcpkg(srcpkg, distro, build_dir)


@cli_prep.command()
@click.option('--srcpkg', '-s', type=Path,
              help="use this source package as input")
@click.option('--apkg', '-a', type=Path,
              help="use this apkg-managed project as input")
@click.option('--repo', '-r', type=str, required=True,
              help="get distros from selected repo mirrors config")
@click.option('--mirrors-dir', '-M', type=Path,
              default=DEFAULT_MIRRORS_PATH, show_default=True,
              help="output source packages for mirrors into this dir")
@click.help_option('-h', '--help', help='show this help')
def mirrors(srcpkg=None, apkg=None, repo=None, mirrors_dir=None):
    """
    prepare per-distro source packages for mirrors
    """
    if not (srcpkg or apkg):
        raise ex.InvalidInput(msg="Please use '--srcpkg' or '--apkg' option to select input.")

    if not mirrors_dir:
        mirrors_dir = DEFAULT_MIRRORS_PATH

    repo_cfg = config_util.get_repo_config(repo)
    for mirror, mcfg in repo_cfg['mirrors'].items():
        mirror_path = mirrors_dir / mirror
        mirror_path.mkdir(parents=True, exist_ok=True)
        for distro in mcfg['distros']:
            if apkg:
                srcpkg = get_apkg_srcpkg(distro, apkg)
            prep_srcpkg(srcpkg, distro, mirror_path)

    return


def get_apkg_srcpkg(distro, project_dir):
    apkg_distro = distro_info.series_apkg_id(distro)
    cmd = ['apkg', 'srcpkg', '-d', apkg_distro]
    o = run(cmd, cwd=project_dir, stdout=subprocess.PIPE, check=True)
    out = o.stdout.decode()
    srcpkg = out.splitlines()[0]
    return project_dir / srcpkg


def prep_srcpkg(srcpkg, distro, result_dir):
    srcpkg_path = srcpkg.absolute()
    print("input source package: %s" % srcpkg)
    dsc = deb822.Dsc(srcpkg_path.open('rt'))
    source = dsc['Source']
    version = dsc['Version']
    # strip release
    version, _, _ = version.partition('-')
    srcpkg_dir = '%s-%s' % (source, version)

    tmp_debian_path = result_dir / 'arepo-debian'
    tmp_debian_path.mkdir(exist_ok=True)
    tmp_source_path = tmp_debian_path / srcpkg_dir

    print("distro: %s" % distro)
    if tmp_source_path.exists():
        print("removing existing source: %s" % tmp_source_path)
        shutil.rmtree(tmp_source_path)
    print("unpacking source into: %s" % tmp_source_path)
    run(['dpkg-source', '-x', srcpkg_path], cwd=tmp_debian_path, check=True)

    with common.cd(tmp_source_path):
        print("updating debian/changelog for %s" % distro)
        ch = changelog.Changelog(open('debian/changelog', 'r'))
        # change distro in changelog
        ch.distributions = distro
        # add ~distro to version
        ch.version = "%s~%s" % (ch.version, distro)
        ch.write_to_open_file(open('debian/changelog', 'w'))

        srcpkg_out_name = '%s_%s.dsc' % (source, ch.version)

        print("building new source package for %s" % distro)
        run(['dpkg-buildpackage',
            '-S',   # source-only, no binary files
            '-sa',  # source includes orig, always
            '-d',   # do not check build dependencies and conflicts
            '-nc',  # do not pre clean source tree
            '-us',  # unsigned source package.
            '-uc',  # unsigned .changes file.
            ], check=True)

    tmp_srcpkg_path = tmp_debian_path / srcpkg_out_name
    if not tmp_srcpkg_path.exists():
        raise ex.UnexpectedCommandOutput(
            msg="New source package not found: %s" % tmp_srcpkg_path)

    print("copying source package to result dir: %s" % result_dir)
    deb.copy_deb_srcpkg(tmp_srcpkg_path, result_dir)

    print("cleaning up temp debian dir: %s" % tmp_debian_path)
    shutil.rmtree(tmp_debian_path)


AREPO_CLI_COMMANDS = [cli_prep]
