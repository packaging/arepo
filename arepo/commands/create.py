import click

from arepo import aaptly
from arepo.config_util import get_repo_config
from arepo import distro_info
from arepo.run import run


@click.command(name='create')
@click.argument('repo', type=str)
@click.option('--comment', '-c', type=str,
              help="repo description")
@click.help_option('-h', '--help', help='show this help')
def create(repo, comment=None):
    """
    create per-distro repos using aptly
    """
    repo_cfg = get_repo_config(repo)
    if not comment:
        comment = repo_cfg.get('name')

    for distro in repo_cfg['distro_lists']['all']:
        local_repo = aaptly.distro_repo(repo, distro)
        args = ['repo', 'create']
        args.append('-distribution=%s' % distro)
        args.append('-component=main')
        if comment:
            series_str = distro_info.series_full_name(distro)
            comment_full = '%s for %s' % (comment, series_str)
            args.append('-comment=%s' % comment_full)
        args.append(local_repo)
        cmd = aaptly.command(args)
        run(cmd, check=False)


AREPO_CLI_COMMANDS = [create]
