#!/usr/bin/env python3
import setuptools

from arepo.__about__ import __version__


setuptools.setup(
    name='arepo',
    version= __version__,
)
