# arepo

Packaging helper tool to automate management of per-distro Debian/Ubuntu repos
using `aptly` with common configuration.

`arepo` can:

* automate `aptly` commands to manage per-distro repos
* import packages from mirrors including Launchpad
* use `pbuilder`/`qemu` to build packages
* modify source packages for multi-distro use
* search and remove packages across repos with nice formatting


## Status

**beta**

Used to manage **one** production packaging machine :)


## Showcase

After `arepo` is installed and configured:

```
arepo create my-repo

arepo prep all -r my-repo -s ~/path/to/srcpkg/mypkg_1.0-1.dsc
cd build
arepo build
arepo add my-repo

arepo search -r my-repo
arepo publish --init my-repo
```


## Commands

```
  add      auto-add .deb packages to per-distro repos using aptly
  build    build packages from source packages using pbuilder
  config   print/export arepo configuration as YAML/JSON/TOML
  create   create per-distro repos using aptly
  drop     [!] delete per-distro repos using aptly
  image    create/update build images (base.tgz) using pbuilder
  import   import packages from mirrors into repos
  list     list repos
  mirror   manage per-distro mirrors using aptly
  prep     prepare per-distro source packages for build and/or mirrors
  publish  publish per-distro repos using aptly
  remove   [!] remove packages in local repos matching aptly query
  search   search packages in local repos matching aptly query
```


## Installation

`arepo` is using modern python packaging, you should be able to install it as
any other python module.

### Install from upstream repo

The standard way of installation is using `pipx` which installs into isolated environment while only exposing `arepo` script:

```
pipx install git+https://gitlab.nic.cz/packaging/arepo
```

You can use `pip` and other packaging tools as well.


### Install local copy

```
git clone https://gitlab.nic.cz/packaging/arepo
cd arepo
pipx install .
```

On Debian 12 and newer (or equivalent clone), you can use [apkg] to build and
install `.deb` package from source:

```
git clone https://gitlab.nic.cz/packaging/arepo
cd arepo
apkg build --build-dep
apkg install
```

### Install in editable mode for development

You can immediately test changes to `arepo` sources using editable mode (`-e`):

```
git clone https://gitlab.nic.cz/packaging/arepo
cd arepo
pip install -e .
```

## Configuration

Start with [arepo.toml](arepo.toml) from `arepo` sources and copy it to:

* `/etc/arepo.toml` for system-wide configuration
* `~/.arepo.toml` for user-wide configuration

and modify it to your needs.

`arepo` also looks for `arepo.toml` config file in current directory.

Run

```
arepo config
```

to see what config `arepo` loaded.



## Build Images

When using `arepo build` to build packages, build images (basetgz) for
`pbuilder` are needed.

Config option `arepo.images_dir` specifies base dir containing images:

```
$IMAGES_DIR/$DISTRO-$ARCH-base.tgz
```

You can create/update using images needed in a repo:

```
# create build images
arepo image create -r my-repo

# update build images
arepo image update -r my-repo
```

You can also select images using `--distros` and `--archs` options:

```
arepo image create -d bullseye -a 'amd64 i386'
```

## Build Repos

When using `arepo build` to build packages, `build_repos` can be added to
individual repos's config to allow external dependencies during build, for
example:

```
[[repos.knot-resolver.build_repos]]
url = 'https://pkg.labs.nic.cz/knot-resolver'
components = ['main']
options = { signed-by = '/usr/share/keyrings/cznic-labs-pkg.gpg' }
```

However, this **requires** a `pbuilder` hook `D23buildrepos` provided with `arepo`
to have any effect.

Assuming you have `pbuilder` `HOOKDIR` set up like this (i.e. in `/etc/pbuilderrc`):

```
HOOKDIR=/var/cache/pbuilder/hooks
```

Copy the hook provided with `arepo` to `HOOKDIR`:

```
cp arepo/pbuilder-hooks/D23buildrepos /var/cache/pbuilder/hooks
```

`arepo` generates `build_repos.list` file per-distro and passes it to `pbuilder` using `--inputfile` option.

`D23buildrepos` hook detects `build_repos.list` file during build,
copies it to `/etc/apt/sources.list.d/`, and calls `apt-get update`.


## TODO

* more docs :)


[apkg]: https://apkg.readthedocs.io/en/latest/
